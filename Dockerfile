FROM openjdk:11-jre-slim
RUN \
\
\
\
useradd sonarqube && \
apt update && apt upgrade -y && apt install -y unzip wget curl procps && \
mkdir -p /tmp/sonarqube && \
wget -P /tmp/sonarqube/ https://binaries.sonarsource.com/Distribution/sonarqube/sonarqube-7.9.1.zip && \
cd /tmp/sonarqube && \
unzip sonarqube-7.9.1.zip && \
mv sonarqube-7.9.1 /opt/sonarqube && \
chown -R sonarqube /opt/sonarqube && \
rm -rf /tmp/sonarqube
USER sonarqube
EXPOSE 9000
ENTRYPOINT ["/opt/sonarqube/bin/linux-x86-64/sonar.sh","console"]
